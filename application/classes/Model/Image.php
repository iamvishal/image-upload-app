<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Image extends Model {
 
	private $image_table;
	public function __construct()
	{
		// load database library into $this->db (can be omitted if not required)
		//parent::__construct();
	    $this->image_table = "images";
	}
	
	public function get_all_images()
	{
	    return DB::select()->from($this->image_table)
                     ->execute()->as_array();
	}
	
	public function get_image_by_id($image_id)
	{
	    return DB::select()->from($this->image_table)->where('id', '=', $image_id)
                     ->execute()->as_array();
	}
	
	public function insert_image_info($data)
	{
	    return DB::insert($this->image_table, array('title', 'filename', 'created_at'))
	    ->values($data)->execute();
	}
	
	public function update_image_info($data, $id)
	{
	    return DB::update($this->image_table)->set(array('title' => $data['title'], 'filename' => $data['filename'], 'updated_at' => $data['updated_at']))->where('id', '=', $id)->execute();
	}
	
	public function delete_image_info($image_id)
	{
	    return DB::delete($this->image_table)->where('id', '=', $image_id)->execute();
	}
 
}