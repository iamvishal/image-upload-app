<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Image extends Controller {

	public function action_index()
	{
	    $images = Model::factory('Image')->get_all_images();
	    
	    foreach($images as $key=>$image) {
		$images[$key]['filename'] = URL::base().'upload/'.$image['filename'];
		$images[$key]['orig_name'] = $image['filename'];
	    }
	    
	    $this->response->body(View::factory('images')->set('image_data', $images));
	}
	
	public function action_add_image()
	{
	    $post = Validation::factory( $_POST );
	    $file = Validation::factory( $_FILES );
	    $title = $post['title'];
	    $image_id = isset($post['image_id']) ? $post['image_id'] : null;
	    
	    $file->rule( 'image', array( 'Upload', 'not_empty' ) );
	    $file->rule( 'image', array( 'Upload', 'valid' ) );
	    
	    $response = array();
	    
	    if (empty($_POST)) {
		$response['status'] = 400;
		$response['msg'] = "Data could not be uploaded";
	    } else if ( Request::current()->method() == Request::POST && $post->check() && $file->check() ) {
		// the request is valid, do your processing

		// save the uploaded file with the name 'form' to our destination
		$filename = Upload::save( $file['image'] );
		
		$splitted = explode("/", $filename);
		$splitted_filename = $splitted[count($splitted) - 1];

		if ( $filename === false ) {
		    $response['status'] = 400;
		    $response['msg'] = "File could not be uploaded";
		} else {
		    $data = array(
			'title' => $title,
			'filename' => $splitted_filename,
			'created_at' => date('Y-m-d H:i:s')
		    );
		    if ($image_id) {
			$data['updated_at'] = date('Y-m-d H:i:s');
			$save_status = Model::factory('Image')->update_image_info($data, $image_id);
		    } else {
			$save_status = Model::factory('Image')->insert_image_info($data);
		    }
		    if ($save_status) {
			$response['status'] = 200;
			$data['file_path'] = URL::base().'upload/'.$splitted_filename;
			if ($image_id) {
			    $data['insert_id'] = $image_id;
			} else {
			    $data['insert_id'] = $save_status[0];
			}
			$response['data'] = $data;
			$response['msg'] = "Image uploaded successfully";
		    } else {
			$response['status'] = 400;
			$response['msg'] = "Data could not be uploaded";
		    }
		}
	    } else {
		$response['status'] = 400;
		$response['msg'] = "Data could not be uploaded";
	    }
	    echo json_encode($response);
	}
	
	public function action_delete_image()
	{
	    $post = Validation::factory( $_POST );
	    $image_id = $post['image_id'];
	    $image_data = Model::factory('Image')->get_image_by_id($image_id);
	    $file_name = DOCROOT.'upload/'.$image_data[0]['filename'];
	    $response = array();
	    if (!unlink($file_name)) {
		$response['status'] = 400;
		$response['status'] = "Some error occured, please try agail later";
		 echo json_encode($response);
		 exit;
	    }
	    
	    $del_status = Model::factory('Image')->delete_image_info($image_id);
	    if ($del_status) {
		$response['status'] = 200;
		$response['msg'] = "Record deleted successfully";
	    } else {
		$response['status'] = "Some error occured, please try agail later";
	    }
	    echo json_encode($response);
	}

} // End Image
