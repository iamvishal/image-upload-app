<html>
    <head>
	<title>Image operations</title>
    </head>
    
    <body>
	<header>
	<script src="<?php echo URL::base().'assets/js/jquery-3.3.1.js';?>"></script>
	<link href="<?php echo URL::base().'assets/css/style.css';?>" rel="stylesheet">
	</header>
	<div class="loader" id="loder" style="display: none;"></div>
	<table class="responstable" align="center">
	    <thead>
		<tr>
		    <th>Title</th>
		    <th>File name </th>
		    <th>Thumbnail</th>
		    <th>Date added</th>
		    <th>Action</th>
		</tr>
	    </thead>

	    <tbody>
		<?php if (count($image_data) < 1) {?>
		<tr>
		    <td id="no_results" colspan="5">No results found</td>
		</tr>
		<?php } else {?>
		<?php foreach($image_data as $image) {?>
		<tr>
		    <td><?php echo $image['title']?></td>
		    <td><?php echo $image['orig_name']?></td>
		    <td><img src="<?php echo $image['filename']?>" height="50" width="50"></td>
		    <td><?php echo date('Y-m-d', strtotime($image['created_at']));?></td>
		    <td>
			<a href="javascript:void(0);" class="action_button edit_btn" data-title="<?php echo $image['title']?>" data-imageid="<?php echo $image['id']?>">Edit</a>&nbsp;&nbsp;
			<a href="javascript:void(0);" class="action_button del_btn" data-imageid="<?php echo $image['id']?>">Delete</a>
		    </td>
		</tr>
		<?php } }?>
	    </tbody>
	</table>
	<table align="center">
	    <div class="pop-btn"><a href="javascript:void(0);" id="add_img" class="action_button">Add Image</a></div>
	</table>
	<div id="myModal" class="modal">

	  <!-- Modal content -->
	  <div class="modal-content">
	    <span class="close">&times;</span>	    
	    <div class="pop-hed">
		<form id="image_upload_form" enctype="multipart/form-data">
		    <input type="hidden" id="image_id_edit" name="image_id"/>
		    <div><label>Title</label> <input type="text" name="title" class="image_title" required=""></div><br>
		    <div><label>Image</label> <input type="file" name="image" required=""></div><br>
		    <div><input type="submit" value="submit" class="action_button"></div>
		</form>
	    </div>	    
	  </div>

	</div>
	<script type="text/javascript">
    $(document).ready(function(){
    // Get the modal
	var modal = document.getElementById('myModal');	
	
	$('#add_img').on('click', function() {
	    $("#image_upload_form").find("input[type=text]").val("");
	    modal.style.display = "block";
	});
	
	$('.close').on('click', function() {
	    modal.style.display = "none";
	});
		

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
		modal.style.display = "none";
	    }
	};
	
	$('#image_upload_form').on('submit', function (e) {

          e.preventDefault();
	  $('#loder').show();
	  var data = new FormData($('#image_upload_form')[0]);

          $.ajax({
            type: 'post',
            url: "<?php echo URL::base() . 'index.php/image/add_image/' ?>",
            data: data,
	    cache: false,
            contentType: false,
            processData: false,
	    dataType: "json",
            success: function (response) {
		if (response.status == 200) {		    
		    alert('Image uploaded successfully');
		    if ($('#image_upload_form #image_id_edit').val()) {
			location.reload(true);
		    } else {
			$('#no_results').hide();
			$('.responstable tbody').append("<tr><td>"+response.data.title+"</td><td>"+response.data.filename+"</td><td><img src='"+response.data.file_path+"' height='50' width='50'></td><td>"+response.data.created_at+"</td><td><a href='javascript:void(0);' class='action_button edit_btn' data-title="+response.data.title+" data-imageid="+response.data.insert_id+">Edit</a>&nbsp;&nbsp;<a href='javascript:void(0);' class='action_button del_btn' data-imageid="+response.data.insert_id+">Delete</a></td></tr>");
		    }
		    modal.style.display = "none";
		} else {
		    alert(response.msg ? response.msg : "Some error occured");
		}
            },
	    error: function() {
		alert('Some error occured while processing your request')
	    },
	    complete: function() {
		 $('#loder').hide();
	    }
          });

        });
	
	$('.responstable').on('click','.edit_btn', function() {
	    modal.style.display = "block";
	    var title = $(this).data('title');
	    var image_id = $(this).data('imageid');
	    $('#image_upload_form #image_id_edit').val(image_id);
	     $('#image_upload_form .image_title').val(title);
	     find("input[type=text], textarea").val("");
	});
	
	$('.responstable').on('click', '.del_btn',function() {
	   var image_id = $(this).data('imageid');
	   $('#loder').show();
	   
	   if (confirm('Are you sure you want to delete this image?')) {
	       $.ajax({
		    type: 'post',
		    url: "<?php echo URL::base() . 'index.php/image/delete_image/' ?>",
		    data: {image_id: image_id},
		    dataType: "json",
		    success: function (response) {
			if (response.status == 200) {
			    $(this).closest('tr').remove();
			    alert('Image deleted successfully');
			    location.reload(true);
			} else {
			    alert(response.msg ? response.msg : "Some error occured");
			}
		    },
		    error: function() {
			alert('Some error occured while processing your request')
		    },
		    complete: function() {
			 $('#loder').hide();
		    }
		  });
	    } else {
		$('#loder').hide();
	    }
	});

      });
	</script>
    </body>
    <footer>
	
    </footer>
</html>